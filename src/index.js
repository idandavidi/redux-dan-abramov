import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import TodosAppReducer from './reducers';
import TodoApp from './components/TodoApp.js';

let store = createStore(TodosAppReducer);

render(
  <Provider store={store}>
    <TodoApp />
  </Provider>,
  document.getElementById('root')
);
