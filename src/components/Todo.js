// Represent a single todo item on the list
import React from 'react';
import { connect } from 'react-redux';
import Actions from '../actions';

const Todo = ({onClick, completed, text}) => (
  <li onClick={onClick}
      style={{textDecoration: completed ? 'line-through' : 'none'}} >
    {text}
  </li>
);

const mapStateToProps = (state, ownProps) => {
  return {
    onClick: ownProps.onClick,
    completed: ownProps.completed,
    text: ownProps.text
  };
}

export default connect(mapStateToProps, null)(Todo);
