// Responsible for main view
import React from 'react';
import TodoList from './TodoList.js';
import AddTodoBar from './AddTodoBar.js';
import VisibilityFilterBar from './VisibilityFilterBar.js';
import { connect } from 'react-redux';
import Actions from '../actions';

let nextTodoId = 0;

class TodoApp extends React.Component {
  render() {
    const visibileTodos = getVisibleTodo(this.props.todos, this.props.visibilityFilter);

    return (
      <div>
        <VisibilityFilterBar
          visibilityFilter={this.props.visibilityFilter}
          onFilterClick={filter =>
             this.props.setVisibilityFilter(filter)
          }
        />

        <AddTodoBar
          onAddClick={text =>
            this.props.addTodo(text)
          }
        />

        <TodoList
          todos={visibileTodos}
          onTodoClick={id =>
            this.props.toggleTodo(id)
          }
        />
      </div>
    );
  }
}

const getVisibleTodo = (todos, filter) => {
  switch (filter) {
    case 'SHOW_ALL':
      return todos;
    case 'SHOW_COMPLETED':
      return todos.filter(t=>t.completed);
    case 'SHOW_ACTIVE':
      return todos.filter(t=>!t.completed);
    default:
      return todos;
  }
};

const mapStateToProps = (state) => {
  return {
    todos: state.todos,
    visibilityFilter: state.visibilityFilter
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    toggleTodo: (id) => {
      dispatch(Actions.toggleTodo(id));
    },
    addTodo: (text) => {
      dispatch(Actions.addTodo(text));
    },
    setVisibilityFilter: (filter) => {
      dispatch(Actions.setVisibilityFilter(filter));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoApp);
