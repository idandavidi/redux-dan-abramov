// Represents a todo list
import React from 'react';
import Todo from './Todo.js';
import { connect } from 'react-redux';
import Actions from '../actions';

const TodoList = ({todos, onTodoClick}) => (
  <ul>
    {todos.map(todo =>
      <Todo key = {todo.id}
        {...todo}
        onClick = {() => onTodoClick(todo.id)} />
    )}
  </ul>
);

const mapStateToProps = (state, ownProps) => {
  return {
    todos: ownProps.todos,
    onTodoClick: ownProps.onTodoClick
  };
}

export default connect(mapStateToProps, null)(TodoList);
