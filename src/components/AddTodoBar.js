// Responsible for add todo bar
import React from 'react';
import { connect } from 'react-redux';
import Actions from '../actions';

const AddTodoBar = ({onAddClick}) => {
  let input;

  return (
    <div>
      <input ref={node => {
        input = node;
      }} />

      <button onClick = {() => {
        onAddClick(input.value);
        input.value = '';
      }}>
        Add todo
      </button>
    </div>
  )
}

const mapStateToProps = (state, ownProps) => {
  return {
    onAddClick: ownProps.onAddClick
  };
}

export default connect(mapStateToProps, null)(AddTodoBar);
