// Responsible for add todo bar
import React from 'react';
import FilterLink from './FilterLink.js';
import { connect } from 'react-redux';
import Actions from '../actions';

const VisibilityFilterBar = ({visibilityFilter, onFilterClick}) => {
  return(
    <p>
      Show: {' '}
      <FilterLink filter="SHOW_ALL" currentFilter={visibilityFilter} onClick={onFilterClick}>All</FilterLink> {' '}
      <FilterLink filter="SHOW_ACTIVE" currentFilter={visibilityFilter} onClick={onFilterClick}>Active</FilterLink> {' '}
      <FilterLink filter="SHOW_COMPLETED" currentFilter={visibilityFilter} onClick={onFilterClick}>Completed</FilterLink>
    </p>
  )
}

const mapStateToProps = (state, ownProps) => {
  return {
    visibilityFilter: ownProps.visibilityFilter,
    onFilterClick: ownProps.onFilterClick
  };
}

export default connect(mapStateToProps, null)(VisibilityFilterBar);
