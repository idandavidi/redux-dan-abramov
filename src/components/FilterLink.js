// Responsible for options of visibility filter choose bar
import React from 'react';
import { connect } from 'react-redux';
import Actions from '../actions';

const FilterLink = ({ filter, currentFilter, children, onClick }) => {
  if (filter === currentFilter) {
    return <span> {children} </span>;
  }

  return (
    <a href='#'
       onClick = { e => {
         e.preventDefault();
         onClick(filter);
       }}
    >
      {children}
    </a>
  );
}

const mapStateToProps = (state, ownProps) => {
  return {
    filter: ownProps.filter,
    currentFilter: ownProps.currentFilter,
    children: ownProps.children,
    onClick: ownProps.onClick
  };
};

export default connect(mapStateToProps, null)(FilterLink);
