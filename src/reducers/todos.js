// State = todos array
// Action = an action to commit on state (with expected todo id)
let deepFreeze = require('deep-freeze');
let expect = require('expect');
let TodoReducer = require('./todo.js');

export default function todos (state = [], action) {
  switch(action.type) {
    case 'ADD_TODO':
      return [
        ...state,
        TodoReducer(undefined, action)
      ];
    case 'TOGGLE_TODO':
      return state.map(todo => TodoReducer(todo, action));
    default:
      return state;
  }
};

const testAddTodo = () => {
  const stateBefore = [];
  const action = {
    type: 'ADD_TODO',
    id: 0,
    text: 'Learn Redux'
  };
  const stateAfter = [
    {
      id: 0,
      text: 'Learn Redux',
      completed: false
    }
  ];

  deepFreeze(stateBefore);
  deepFreeze(action);

  expect(todos(stateBefore, action)).toEqual(stateAfter);
}

const testToggleTodo = () => {
  const stateBefore = [
    {
      id: 0,
      text: 'Learn Redux',
      completed: false
    }, {
      id: 1,
      text: 'Go shopping',
      completed: false
    }
  ];

  const action = {
    type: 'TOGGLE_TODO',
    id: 1
  }

  const stateAfter =  [
    {
      id: 0,
      text: 'Learn Redux',
      completed: false
    }, {
      id: 1,
      text: 'Go shopping',
      completed: true
    }
  ];

  deepFreeze(stateBefore);
  deepFreeze(action);

  expect(todos(stateBefore, action)).toEqual(stateAfter);
}

testAddTodo();
testToggleTodo();
console.log('All TodosReducer tests has passed.');
