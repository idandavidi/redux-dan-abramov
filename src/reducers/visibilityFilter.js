// State = filter state string
// Action = action to commit on filter
let deepFreeze = require('deep-freeze');
let expect = require('expect');

export default function visibilityFilter (state = 'SHOW_ALL', action) {
  switch(action.type) {
    case 'SET_VISIBILITY_FILTER':
      return action.filter;

    default:
      return state;
  }
};

const testSetVisibilityFilter = () => {
  const stateBefore = undefined;
  const action = {
    type: 'SET_VISIBILITY_FILTER',
    filter: 'SHOW_COMPLETED'
  };
  const stateAfter = 'SHOW_COMPLETED';

  // deepFreeze(stateBefore); // deepFreeze of undefined is not allowed
  deepFreeze(action);

  expect(visibilityFilter(stateBefore, action)).toEqual(stateAfter);
}

const testDefaultValue = () => {
  const stateBefore = undefined;
  const action = {
    type: 'Unknown'
  };
  const stateAfter = 'SHOW_ALL';

  // deepFreeze(stateBefore); // deepFreeze of undefined is not allowed
  deepFreeze(action);

  expect(visibilityFilter(stateBefore, action)).toEqual(stateAfter);
}

testSetVisibilityFilter();
testDefaultValue();
console.log('All VisibilityFilterReducer tests has passed.');
