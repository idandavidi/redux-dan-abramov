// This is the orchestra of all reducers
// State = object of todos array and visibilityFilter string.
// Action = an action to commit on todos or on visibilityFilter.
import TodosReducer from './todos.js';
import VisibilityFilterReducer from './visibilityFilter.js';

export default function TodosAppReducer (state = {}, action)  {
  return {
    todos: TodosReducer(state.todos, action),
    visibilityFilter: VisibilityFilterReducer(state.visibilityFilter, action)
  };
};
